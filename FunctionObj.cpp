
class ConnectedClient
{
public:
	ConnectedClient(int id):id(id)
	{}
	~ConnectedClient(){}

	int id;
};

// find
class client_finder
{
public:
	client_finder( const int id ):m_id(id)
	{}

	bool operator() ( const ConnectedClient *ptr )
	{
		return ptr->id == m_id;
	}
private:
	int m_id;
};

// test case
int clientid;
std::vector<ConnectedClient * > vec;
vec.find_if(vec.begin(),vec.end(),client_finder(clientid));