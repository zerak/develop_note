#!/bin/bash

#注：本脚本需要以超级用户身份运行。

# 监测的时间间隔，秒计
INTERVAL=1

# 重启时间间隔
INTERVALRESTART=1

#==================================================================

PROGRAME=txtj_authenticationserver
THREADNUMS=1
LOG=/home/txtj/monitor.log

var="-e"

while true
do
   SYSDATE=$(date)

   nowps1=`ps $var | grep $PROGRAME | grep -v grep | wc -l`
   nowps1=`expr $nowps1`

   if  [  $nowps1 -lt $THREADNUMS ]; then 
        #/bin/sh 
	/home/txtj/txtj_authenticationserver &
        echo " " >> $LOG
        echo "*******************************************************" >> $LOG
        echo "Restart time:" $SYSDATE >> $LOG
        echo "---------- Program $PROGRAME restart ----------------" >> $LOG
        echo "*******************************************************" >> $LOG
        echo " "
   fi
sleep $INTERVAL
   
done

#-----------------------------------------------------------
exit 0

